# `mus-learn' - LuaLaTeX Machine Learning Algorithms Showcases

The `mus-learn` bundle provides several packages developed
during GENIOMHE course on machine learning in LuaLaTeX as
a coding challenge and automation of exercises answering in
tutorials.

## Installation

The package is supplied in `.dtx` format and as pre-extracted `.zip` file `mus-learn.dtx.zip`.

ref. https://github.com/josephwright/chemstyle/blob/main/chemstyle.dtx

\RequirePackage{luatextra}
\RequirePackage{luatexbase}
\RequirePackage{luacode}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\begin{luacode}
  local mus_kmeans = require('mus-learn.kmeans')
\end{luacode}
\luadirect{kmeans_view = require("mus-learn.views.kmeans")}
\newcommand{\kmeansAlgoTikz}[2]{
  \luadirect{
    local data = \luastring{#1}
    local k = \luastring{#2}
    local tikz_code = kmeans_view.tikz(data, k)
    print(tikz_code)
    tex.print(tikz_code)
  }
}

\newcommand\kmeansAlgoSteps[1]{
  \luadirect{
    data = \luastringN{#1}
    local latex_table = kmeans_view.repr(data)
    tex.print(latex_table)
    print(latex_table)
  }
}

