local latex_table = require("mus-learn.views.latex_table")
local mus_kmeans = require("mus-learn.kmeans")

local function split(str, sep)
    local result = {}
    local regex = ("([^%s]+)"):format(sep)
    for each in str:gmatch(regex) do
        table.insert(result, each)
    end
    return result
end

-- Retrieve latex argument variable as string
-- Parse the variable as points
local function parse(data)
    local labels = {}
    local values = {}

    local lines = split(data, ";")
    for i, line in ipairs(lines) do
        local record_fields = split(line, ",")
        local record_label = record_fields[1]
        local record_value = tonumber(record_fields[2])
        labels[i] = record_label
        values[i] = record_value
    end
    return { labels = labels, values = values }
end


local function kmeans_algo_representation(data)
    local records = parse(data)
    local record_table = { records["labels"], records["values"] }
    return latex_table.table_to_latex(record_table)
end


local function kmeans_tikz_still(kmeans, values, y_shift, valuemin, valuemax, xmin, xmax)
    local function scale_value(value)
        return ((xmax - xmin) / (valuemax - valuemin)) * value
    end
    local y_shift = y_shift or 0
    local tikz_code = ""
    -- scaled values
    local scaled_values = {}
    for i, value in ipairs(values) do
        scaled_values[i] = scale_value(value)
    end
    -- Draw the cluster 
    for i, mean in ipairs(kmeans) do
        local scaled_mean = scale_value(mean) 
        tikz_code = tikz_code .. "\\node[draw,circle,blue,fill,fill opacity=0.5] at (" .. scaled_mean .. ", " .. y_shift .. ") {};"
    end
    -- Draw the values
    for i, scaled_value in ipairs(scaled_values) do
        tikz_code = tikz_code .. "\\node[black] at (" .. scaled_value .. ",".. y_shift .. ") {.};"
    end
    return tikz_code
end


local function kmeans_tikz_stills(data, k, xmin, xmax)
    local xmin = xmin or 0
    local xmax = xmax or 5
    local parsed_data = parse(data)
    local labels = parsed_data["labels"]
    local values = parsed_data["values"]
    local k = k or 2
    local values_extremum = mus_kmeans.value_extremum(values)
    local kmeans = mus_kmeans.random_init_kmeans(k, values_extremum.min, values_extremum.max)
    local tikz_code = ""
    local y_shift = 0
    local previous_kmeans = kmeans
    repeat
        previous_kmeans = kmeans
        tikz_code = tikz_code .. kmeans_tikz_still(kmeans, values, y_shift, values_extremum.min, values_extremum.max, xmin, xmax)
        local clusters = mus_kmeans.voronoi_partition(mus_kmeans.manhattan_distance, previous_kmeans, values)
        kmeans = mus_kmeans.update_kmeans(clusters, values)
        y_shift = y_shift + 1
    until (mus_kmeans.arrayEqual(previous_kmeans, kmeans))
    tikz_code = [[\begin{tikzpicture}]] .. tikz_code .. [[\end{tikzpicture}]]
    return tikz_code
end


return {
    repr = kmeans_algo_representation,
    tikz = kmeans_tikz_stills
}
