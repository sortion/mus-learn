-- Some utilities for LuaLaTeX tables
-- Copyright (c) 2023 Samuel Ortion

local function table_to_latex(table)
    local latex = "\\begin{tabular}{"
    for _ = 1,#table[1] do
        latex = latex .. "c"
    end
    latex = latex .. "}\n"
    for i = 1,#table do
        for j = 1,#table[i] do
            latex = latex .. table[i][j]
            if j ~= #table[i] then
                latex = latex .. " & "
            end
        end
        latex = latex .. " \\\\ \n"
    end
    latex = latex .. "\\end{tabular}"
    return latex
end


local function matrix_to_latex(table, nillable)
    local latex = "\\begin{pmatrix}\n"
    for i = 1,#table do
        for j = 1,#table[i] do
            if table[i][j] == -1 then
                if nillable then
                    latex = latex .. "\\texttt{nil}"
                else
                    latex = latex .. table[i][j]
                end
            else
                if table[i][j] == math.huge then
                    latex = latex .. "\\infty"
                else
                    latex = latex .. table[i][j]
                end
            end
            if j ~= #table[i] then
                latex = latex .. " & "
            end
        end
        if i == #table then
            latex = latex .. " \n"
        else
            latex = latex .. " \\\\ \n"
        latex = latex .. " \\\\ \n"
    end
    end
    latex = latex .. "\\end{pmatrix}"
    return latex
end

return {
    table_to_latex = table_to_latex,
    matrix_to_latex = matrix_to_latex
}