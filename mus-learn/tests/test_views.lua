local kmeans_view = require("mus-learn.views.kmeans")
local mus_kmeans = require("mus-learn.kmeans")

local raw_data = [[
%$g_1$,10;
$g_2$,12;
$g_3$,9;
$g_4$,15;
$g_5$,17;
$g_6$,18
]]

local tikz_code = kmeans_view.tikz(raw_data, {1, 5, 10})
print(tikz_code)

local extremum = mus_kmeans.value_extremum({1, 3, 2})
print(extremum.min, extremum.max)