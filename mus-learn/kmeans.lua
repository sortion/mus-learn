-- k-means algorithm

local function random_init_kmeans(k, min, max)
    local kmeans = {}
    for i = 1, k do
        local kmean = (max - min) * math.random() + min
        kmeans[i] = kmean
    end
    return kmeans
end


-- Compute manhattan distance on two vector (or scalar) of same dimension recursively
local function manhattan_distance(u, v)
    local d = 0
    if type(u) == "table" and type(v) == "table" then
        if #u ~= #v then
            error("u and v does not have the same dimension, unable to compute manhattan_distance")
        else
            for i in 1, #u do
                d = d + manhattan_distance(u[i], v[i])
            end
        end
    else
        d = d + math.abs(u - v)
    end
    return d
end


local function arrayEqual(a1, a2)
    -- Check length, or else the loop isn't valid.
    if #a1 ~= #a2 then
        return false
    end

    -- Check each element.
    for i, v in ipairs(a1) do
        if v ~= a2[i] then
            return false
        end
    end

    -- We've checked everything.
    return true
end


-- Partitition the records vectors in cluster
-- based on the minimal distance value computed with given distance function
local function voronoi_partition(distance, kmeans, records)
    local partition = {}
    for record_index = 1, #records do
        local record = records[record_index]
        local minimal_distance = math.huge
        local nearest_mean_index = 1
        for mean_index = 1, #kmeans do
            local mean = kmeans[mean_index]
            local d = distance(record, mean)
            if nearest_mean_index == nil or d < minimal_distance then
                minimal_distance = d
                nearest_mean_index = mean_index
            end
        end
        if partition[nearest_mean_index] == nil then
            partition[nearest_mean_index] = {}
        end
        table.insert(partition[nearest_mean_index], record_index)
    end
    return partition
end


-- kmeans is a table of values
-- clusters is a table of table of records index
-- records is a table of values
local function update_kmeans(clusters, records)
    local kmeans = {}
    for kmean_index, cluster in ipairs(clusters) do
        local sum = 0
        for _, record_index in ipairs(cluster) do
            sum = sum + records[record_index]
        end
        local mean = sum / #cluster
        kmeans[kmean_index] = mean
    end
    return kmeans
end


local function kmeans_step(kmeans, records)
    local cluster = voronoi_partition(manhattan_distance, kmeans, records)
    local next_kmeans = update_kmeans(cluster, records)
    return next_kmeans
end


local function kmeans_loop(records, k)
    local kmeans = random_init_kmeans(k)
    repeat
        local next_kmeans = kmeans_step(kmeans, records)
    until (arrayEqual(next_kmeans, kmeans))
    return kmeans
end


local function main()
    local k = 2
    local kmeans = random_init_kmeans(k, 1, 10)
    print(table.concat(kmeans, ", "))
    local records = { 1, 5, 6, 10, 15 }
    local partition = voronoi_partition(manhattan_distance, kmeans, records)
    for cluster_index = 1, #partition do
        io.write(cluster_index, ": ")
        for clustered_index = 1, #partition[cluster_index] do
            local record_index = partition[cluster_index][clustered_index]
            local record = records[record_index]
            io.write(record)
            if clustered_index < #partition[cluster_index] then
                io.write(", ")
            end
        end
        print()
    end
end


local function value_extremum(A)
    if #A == 0 then
        error("Cannot get extremum values from empty table")
    end
    local minimum = A[1]
    local maximum = A[1]
    for i = 1,#A do
        if A[i] > maximum then
            maximum = A[i]
        elseif A[i] < minimum then
            minimum = A[i]
        end
    end
    return {
        min = minimum,
        max = maximum
    }
end

-- main()

return {
    kmeans = kmeans_loop,
    kmeans_step = kmeans_step,
    update_kmeans = update_kmeans,
    random_init_kmeans = random_init_kmeans,
    voronoi_partition = voronoi_partition,
    manhattan_distance = manhattan_distance,
    arrayEqual = arrayEqual,
    value_extremum = value_extremum
}