target=mus-learn
TEXMFHOME=$(shell kpsewhich -var-value=TEXMFHOME)
LATEXHOME=$(TEXMFHOME)/tex/latex/
LUAHOME=$(TEXMFHOME)/scripts/

all: docstrip doc

docstrip: 
	tex $(target).dtx

doc: 
	lualatex -shell-escape -file-line-error $(target).dtx

install: docstrip
	cp *.sty $(LATEXHOME)
	cp -r ./mus-learn $(LUAHOME)
